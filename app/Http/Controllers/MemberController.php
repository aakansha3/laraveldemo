<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{
    //
    function memberAdd(Request $req){
        $member = new Member;
        $member->name=$req->name;
        $member->email=$req->email;
        $member->password=$req->password;
        $member->save();
        Session::flash('success','Email sent Successfully');        
        return view('member') ;
    }
}
