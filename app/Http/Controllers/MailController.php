<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Mail\DemoMail;
use Illuminate\Http\Request;
use App\Jobs\Payment;
use App\Mail\PaymentMail;
use Illuminate\Support\Facades\Session;
class MailController extends Controller
{
    //
    // public function index(){
    //     $mailData = [
    //         'title'=> 'Testing mail From laravel',
    //         'body'=>'This mail is for testing from laravel'
    //     ];
    //     Mail::to('aakanshakarena36@gmail.com')->send(new DemoMail($mailData));
    //     dd("Email send successfully");
    // }
    public function sendMail(Request $req){       
        $to = $req->input('to');                     
        $subject = $req->input('subject');
        $message = $req->input('message');         
        // $job= new Payment($to,$subject, $message);        
        // dispatch($job); 
        Mail::to($to)->queue(new PaymentMail($subject,$message));
        Session::flash('success', 'Email sent successfully!');
        return redirect()->route('send-mail');               
    }
    
}
