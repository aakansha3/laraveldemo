<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormData;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\SharkController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\MemberController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Route::view('/about','about');
Route::view('/contact','contact');
Route::post('/insert',[FormData::class,'insertData']);
Route::get('/Product',[ApiController::class,'getProductData']);
Route::view('crud','crud');
Route::get('crud',[FormData::class,'showlist']);
Route::get('delete/{id}',[FormData::class,'delete']);
Route::get('update/{id}',[FormData::class,'edit']);
Route::post('update/{id}',[FormData::class,'update']);
Route::view('translate', 'translate')->name('translate');
// Route::view('translate','translate');
Route::view('crudscaffold','crudscaffold');
// Route::get('{lang}',function($lang){
//     App::setlocale($lang);
//     return view('translate');
// });
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
Route::resource('sharks', SharkController::class);
// Route::get('send-mail',[MailController::class,'index']);
Route::view('send-mail','mailForm')->name('send-mail');
Route::post('send-mail', [MailController::class, 'sendMail'])->name('send-mail.post');
Route::view('member','member');
Route::post('member',[MemberController::class,'memberAdd'])->name('member.post');
Route::view('users','users');
Route::get('users',[PostController::class,'index']);
Route::get('/users/delete/{id}',[PostController::class,'postdelete']);
Route::get('/gate',[App\Http\Controllers\AuthorizeController::class,'index'])->name('gate.index');
Route::get('/posts',[App\Http\Controllers\PostController::class,'index'])->name('posts.index');
require __DIR__.'/auth.php';

