<?php

namespace Database\Seeders;
use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Member::create([
            'name'=>'Aakansha',
            'email'=>'aakanshakarena36@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
        Member::create([
            'name'=>'Mahesha',
            'email'=>'maheshakarena@gmail.com',
            'password'=>Hash::make('12345678')
        ]);
    }
}
