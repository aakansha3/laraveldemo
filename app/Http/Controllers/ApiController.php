<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class ApiController extends Controller
{
    //
    function getProductData(){
        $response = Http::get("https://fakestoreapi.com/products");
        if($response->successful()){
            $ProductData = $response->json();
            return view('product',['ProductData'=>[$ProductData]]);
        }
        else{
            abort ($response->status());
        }
    }
}
