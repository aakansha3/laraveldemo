<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
@if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <div class="container my-5">
    {{Form::open(array('route'=>'member.post','method'=>'post'))}}
@csrf 
<div class="form-group mb-3">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control','placeholder'=>'Enter your Name')) }}
    </div>   
<div class="form-group mb-3">
        {{ Form::label('email', 'Email')}}
        {{ Form::email('email', null, array('class' => 'form-control','placeholder'=>"Enter your Email")) }}
    </div>
    
    <div class="form-group mb-3">
        {{ Form::label('password', 'Password') }}
        {{ Form::text('password', null, array('class' => 'form-control','placeholder'=>'Password')) }}
    </div>
    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
    </div>



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</body>
</html>