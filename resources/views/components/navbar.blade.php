<style>
        li{
            margin-left:30px;
            margin-top:-7px;
        }
        ul{
            list-style-type:none;
            display:flex;   
                     
        }
        nav{
            background:black;
            color:white;
            height:35px;
            width:100%;
            position:absolute;
            top:0px;
        }
        a{
            text-decoration:none;
            color:white;
        }
    </style>
<div>
<nav class="navbar">
        <ul>
            <li><a href="/welcome">Home</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/contact">Contact us</a></li>
            <li><a href="/Product">Product</a></li>  
            <li><a href="/translate">Translate</a></li>           
            <li><a href="/crud">CRUD</a></li>
            <li><a href="/send-mail">Send Mail</a></li>
            <li><a href="/member">Member</a></li>
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>       
        </ul>
    </nav>
    <!-- No surplus words or unnecessary actions. - Marcus Aurelius -->
</div>