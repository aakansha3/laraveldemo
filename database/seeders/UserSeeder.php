<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\Post;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table("users")->insert([
            'name'=>"Mahesha",
            'email'=>'Mahesha@gmail.com',
            'password'=>Hash::make('12345678'),
            'role'=>'users' 
        ]);
        // Post::factory(8)->create();
    }
}
