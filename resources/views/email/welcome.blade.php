<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mail Sending</title>
</head>
<body>
<p>Hello {{ $name }},<p>

<p>Welcome to our website! We're excited to have you as a member.</p>

<p>Thank you for joining us.</p>

<p>Regards,<br>  
The Website Team.</p>   
</body>
</html>