<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .container {
        position: absolute;
        top: 50px;
        left: 50px;
    }
    a {
        color: black; /* Change the color if needed */
        text-decoration: underline; /* Underline the link text */
    }
    </style>
</head>

<body>
    <x-navbar />   
    {{-- <div class="container">
        <form action="insert" method="post">
        @csrf        
        <h3>Add Member</h3>        
        <input type="text" name="name" placeholder="Enter Your Name"/><br><span style="color:red">@error('name'){{$message}}@enderror</span><br>
    <input type="email" name="email" placeholder="Enter Your Email" /><br><span
        style="color:red">@error('email'){{$message}}@enderror</span><br>
    <input type="text" name="city" placeholder="Enter Your City" /><br><span
        style="color:red">@error('city'){{$message}}@enderror</span><br>
    <button type="submit">Submit</button>
    </form>
    </div> --}}
 
    {{--<div class="container">       
    {!! Form::open(['url'=>'insert','method'=>'post'])!!}
        {!!Form::text('name','',['type'=>'text','placeholder'=>'Enter your name'])!!}
        {!!Form::text('email','',['type'=>'email','placeholder'=>'Enter your Email'])!!}
        {!!Form::text('city','',['type'=>'text','placeholder'=>'Enter your City'])!!}
        {!!Form::submit('Submit')!!}
        
    {!! Form::close()!!}
    {!!link_to_route('translate', $title = 'Link Text') !!}
    </div>--}}

    <div class="container">
    {!! Form::customForm('insert','Add') !!}
    </div>
</body>

</html>