<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserList;
class FormData extends Controller
{
    //
    function insertData(Request $req){
        $req->validate([
            'name'=>'required | max:20',
            'email'=>'required | max:25',
            'city' => 'required | max:20'
        ]);
        $data = new UserList;
        $data->name=$req->name;
        $data->email=$req->email;
        $data->city=$req->city;
        $data->save();
        return redirect('welcome');
        // return $req->input();
        // return "form data";
    }
    function showlist(){
        $data = UserList::all();
        return view('crud',['Users'=>$data]);
    }
    function delete($id){
        $data = UserList::find($id);
        if ($data) {
            $data->delete();
        }        
        return redirect('crud');  
        
    }
    function edit($id){
        $data =  UserList::find($id);
        // return $data;
        if(!$data){
            abort(404);
        }
        return view('update',['data'=>$data]);    
    }
    function update(Request $req){
        $data = UserList::find($req->id);
        $data->name=$req->name;
        $data->email=$req->email;
        $data->city=$req->city;
        $data->save();
        return redirect('crud');
        // return $req->input();
    }
}
