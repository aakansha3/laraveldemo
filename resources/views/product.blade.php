<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
</head>
<body>
    <h1 align="center">Product List</h1>
    <table border="1">
        <thead>
            <tr>                
                <th>id</th>
                <th>title</th>
                <th>price</th>
                <th>description</th>
                <th>category</th>
                <th>image</th>
                <th>rating</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ProductData[0] as $item)
                <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['title']}}</td>
                    <td>{{$item['price']}}</td>
                    <td>{{$item['description']}}</td>
                    <td>{{$item['category']}}</td>
                    <td><img src="{{$item['image']}}" alt="Product Image" style="width: 100px; height: auto;"></td>
                    <td>{{$item['rating']['rate']}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
