<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Main extends Component
{
    /**
     * Create a new component instance.
     */
    public $title;
    public function __construct($componentName)
    {
        //
        $this->title = $componentName;
        \Log::info('Component Name: ' . $this->componentName);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.main');
    }
}
