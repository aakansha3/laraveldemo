<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>CRUD Operation</h1>
<h1>Member List</h1>
    <table border="1">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>City</th>
                <th>Crud Operation</th>
            </tr>
        </thead>
        <tbody>
            @foreach($Users as $user)
        <tr>
                <td>{{$user['id']}}</td>
                <td>{{$user['name']}}</td>
                <td>{{$user['email']}}</td>
                <td>{{$user['city']}}</td>
                <td><a href="{{'delete/'.$user['id']}}">Delete</a>&nbsp;<a href="{{'update/'.$user['id']}}">Edit</a></td>
        </tr>
        @endforeach
</tbody>
</table>
</body>
</html>