<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .container {
        position: absolute;
        top: 50px;
        left: 50px;
    }
    </style>
</head>

<body>
    <x-navbar />

    {{--<div class="container">
        <h1>Update Member</h1>
        <form action="/update" method="post">
            @csrf
            <input type="hidden" name="id" value="{{$data['id']}}" />
            <input type="text" name="name" value="{{$data['name']}}" /><br><br>
            <input type="email" name="email" value="{{$data['email']}}" /><br><br>
            <input type="City" name="city" value="{{$data['city']}}" /><br><br>
            <button type="submit">Update</button>
        </form>
    </div>--}}

    <div class="container">
    {!! Form::customForm("update/{$data->id}",'Update', ['id' => $data->id, 'name' => $data->name, 'email' => $data->email, 'city' => $data->city]) !!}
    </div>
</body>

</html>