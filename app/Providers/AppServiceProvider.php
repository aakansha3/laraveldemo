<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormFacade as Form;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Form::macro('customForm',function($url,$btn,$data = null){
            $defaultData = [
                'id' => null,
                'name' => '',
                'email' => '',
                'city' => ''
            ];
            if(is_null($data)){
                $data = $defaultData;
            }
            else {
                $data = array_merge($defaultData, $data);
            }
            return Form::open([
                'url' => $url,
                'method' => 'post',                
            ]) .
            '<h1>'.$btn.' Member</h1>' .
            Form::hidden('id', $data['id']) .
            Form::text('name', $data['name'],['placeholder'=>'Enter your Name']) . '<br><br>' .
            Form::email('email', $data['email'],['placeholder'=>'Enter your Email']) . '<br><br>' .
            Form::text('city', $data['city'],['placeholder'=>'Enter your City']) . '<br><br>' .
            Form::submit($btn) .
            Form::close();
        });            
    }
}
