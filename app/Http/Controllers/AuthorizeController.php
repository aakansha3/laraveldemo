<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Gate;
class AuthorizeController extends Controller
{
    //
    function index(){
        // return "it worked for admin";
        Gate::allows('isAdmin') ? Response :: allow():abort(403);
        return "Authorization";
    }
}
