@if(isset($shark))    
    <h1>Edit {{ $shark->name }}</h1>
    {{ Form::model($shark, array('route' => array('sharks.update', $shark->id), 'method' => 'PUT')) }}
@else    
    <h1>Create a shark</h1>
    {{ Form::open(array('url' => 'sharks')) }}
@endif

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name', isset($shark) ? $shark->name : null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'Email') }}
    {{ Form::email('email', isset($shark) ? $shark->email : null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('shark_level', 'shark Level') }}
    {{ Form::select('shark_level', array('0' => 'Select a Level', '1' => 'Sees Sunlight', '2' => 'Foosball Fanatic', '3' => 'Basement Dweller'), isset($shark) ? $shark->shark_level : null, array('class' => 'form-control')) }}
</div>

@if(isset($shark))    
    {{ Form::submit('Edit the shark!', array('class' => 'btn btn-primary')) }}
@else    
    {{ Form::submit('Create the shark!', array('class' => 'btn btn-primary')) }}
@endif
{{ Form::close() }}

<!-- plch cava igef kzwx -->